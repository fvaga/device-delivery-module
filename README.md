The aim of this project is to explore a new way of registering Linux devices.
The idea is to feed this module with user input that tells the module which kind
of device to register and what resources to use.


+---------+  resources  +------+   platform_device   +--------------+
| CARRIER |------------>| CORE |-------------------->| PLATFORM_BUS |
+---------+             +------+                     +--------------+
                            ^
                            | platform_devinfo
                            |
                        +---+----+
                        | PARSER | (SDB, DeviceTree, ...)
                        +--------+
                            ^
                            |
                     user description


The idea is to use the platform bus a base for register our FPGA devices.
The carrier exports the resources (memory, IRQs) and the user provides the
device information that will be used together with the carrier resources to
register a valid device.