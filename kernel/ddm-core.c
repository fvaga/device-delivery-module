/*
 * Copyright (C) 2016 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version
 *
 * TODO pin the resource owner (carrier) when the resource is used
 * TODO before remove a resource check if it's in use
 */

#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/spinlock.h>
#include <linux/list.h>
#include <linux/slab.h>

#include "ddm.h"

struct ddm_status {
	struct list_head list_base_res;
	spinlock_t lock;
};

static struct ddm_status ddm_global_status;
static struct ddm_status *ddm = &ddm_global_status;

static void ddm_info(struct ddm_device_user *ddmdev)
{
	struct ddm_resource_user *r;
	int i;

	for (i = 0; i < DDM_REGISTRATION_RES_MAX_LEN; i++) {
		if (!(ddmdev->res[i].flags & IORESOURCE_TYPE_BITS))
			break;
		r = &ddmdev->res[i];
		pr_info("%s %s.%d [%s.%d (%s.%d) [mem 0x%llx-0x%llx flags 0x%lx]]\n",
			__func__,
			ddmdev->name, ddmdev->id,
			r->id.name, r->id.id,
			r->parent.name, r->parent.id,
			r->start, r->end, r->flags);
	}
}


/**
 * It looks for the given resource ID in the resource list
 * @param[in] ddm_id resource id to look for
 * @return it returns the resource corresponding to the given ID. NULL when
 *         the function does not find any resource.
 */
static struct ddm_resource *ddm_resource_find(struct ddm_resource_id *ddm_id)
{
	struct ddm_resource *cur;

	if (!ddm_id)
		return NULL;

	list_for_each_entry(cur, &ddm->list_base_res, list) {
		if (cur->id.id == ddm_id->id &&
		    strcmp(cur->id.name, ddm_id->name) == 0)
			return cur;
	}

	return NULL;
}


/**
 * It counts how many user resources are available
 * @param[in] ddmdev user device declaration
 * @return number of resources
 */
static inline unsigned int ddm_resource_user_count(struct ddm_device_user *ddmdev)
{
	unsigned int count;

	while (ddmdev->res[count].flags & IORESOURCE_TYPE_BITS)
		count++;

	return count;
}


/**
 * It creates a ME resource from a user resource.
 * It inserts the given resource into the resource tree within the
 * given parent resource
 * @param[out] rk Linux kernel resource
 * @param[in] p parent resource
 * @param[in] ru user resource with the information to export
 */
static void ddm_create_resource_mem(struct resource *rk, struct ddm_resource *p,
				    struct ddm_resource_user *ru)
{
	rk->parent = p->res;
	rk->start = p->res->start + ru->start;
	rk->end = p->res->start + ru->end;
	rk->flags = ru->flags;
}


/**
 * It creates an IRQ resource from a user resource.
 * It just takes the resource as is because the IRQs are exported one by one;
 * this means that the device claims the resources directly and not a subset
 * @param[out] rk Linux kernel resource
 * @param[in] p parent resource
 * @param[in] ru user resource with the information to export
 */
static void ddm_create_resource_irq(struct resource *rk, struct ddm_resource *p,
				    struct ddm_resource_user *ru)
{
	memcpy(rk, p->res, sizeof(struct resource));
	rk->parent = p->res;
}


/**
 * It converts an user resource into a kernel resource
 * @param[out] rk kernel resoruce
 * @param[in] ru user resource
 * @return 0 on success, a negative value on error
 */
static inline int ddm_resource_convert(struct resource *rk,
				       struct ddm_resource_user *ru)
{
	struct ddm_resource *p;

	p = ddm_resource_find(&ru->parent);
	if (!p) {
		pr_err("Cannot find parent resource with id \"%s.%d\"\n",
		       ru->parent.name, ru->parent.id);
		return -ENODEV;
	}

	/* TODO to be done when I find a way to module_put on device removal */
	/* ret = try_module_get(p->parent->driver->owner); */
	/* if (ret == 0) { /\* 0 fail, 1 success *\/ */
	/* 	dev_err(p->parent, */
	/* 		"Cannot pin the \"%s\" driver. Something really wrong is going on\n", */
	/* 		p->parent->driver->name); */
	/* 	return -ENODEV; */
	/* } */

	switch (ru->flags & IORESOURCE_TYPE_BITS) {
	case IORESOURCE_MEM:
		ddm_create_resource_mem(rk, p, ru);
		break;
	case IORESOURCE_IRQ:
		ddm_create_resource_irq(rk, p, ru);
		break;
	default:
		pr_err("Unknown resource type 0x%lx\n", ru->flags);
		return -ENODEV;
	}

	pr_info("%s add resource %pr in %pr", __func__, rk, rk->parent);
	return 0;
}


/**
 * It builds the platform_device_info structure from the information
 * provided by the user
 * @param[out] pdevinfo the platform device info structure
 * @param[in] ddmdev the user device information
 * @return 0 on success, a negative number on error. On error the pdevinfo
 *         memory will be cleared (set to 0)
 */
static int ddm_pdev_info_builder(struct platform_device_info *pdevinfo,
				 struct ddm_device_user *ddmdev)
{
	int i, err;

	ddm_info(ddmdev);


	pdevinfo->id = ddmdev->id;
	pdevinfo->num_res = ddm_resource_user_count(ddmdev);

	pdevinfo->name = kstrdup(ddmdev->name, GFP_KERNEL);
	if (!pdevinfo->name)
		return -ENOMEM;
	pdevinfo->res = kzalloc(sizeof(struct resource) * pdevinfo->num_res,
				GFP_KERNEL);
	if (!pdevinfo->res)
		return -ENOMEM;

	/* Convert all user resources */
	for (i = 0; i < pdevinfo->num_res; i++) {
		err = ddm_resource_convert((struct resource *)&pdevinfo->res[i],
					   &ddmdev->res[i]);
		if (err)
			goto out_res;
	}

	return 0;

out_res:
	kfree(pdevinfo->res);
	kfree(pdevinfo->name);
	memset(pdevinfo, 0, sizeof(struct platform_device_info));

	return err;
}


/**
 * This sysfs attribute is used to communicate the information about
 * the device to create and register in kernel space
 *
 * Since it's not possible for a kernel module to create a binary
 * sysfs attribute for a bus, we have to manually check the size of
 * the incoming data.
 */
ssize_t ddm_device_add_store(struct bus_type *bus,
			     const char *buf, size_t count)
{
	struct platform_device_info pdevinfo;
	struct platform_device *pdev;
	int err;

	if (count != sizeof(struct ddm_device_user)) {
		pr_err("%s expecter ddm_structure of size %zu, got %zu\n",
		       __func__, sizeof(struct ddm_device_user), count);
		return -EINVAL;
	}
	memset(&pdevinfo, 0, sizeof(struct platform_device_info));

	err = ddm_pdev_info_builder(&pdevinfo, (struct ddm_device_user *)buf);
	if (err)
		return err;

	pdev = platform_device_register_full(&pdevinfo);

	kfree(pdevinfo.res);
	kfree(pdevinfo.name);

	return IS_ERR_OR_NULL(pdev) ? PTR_ERR(pdev) : count;
}
BUS_ATTR(ddm_device_add, 0220, NULL, ddm_device_add_store);



/**
 * It removes a platform device by ID
 */
ssize_t ddm_device_del_store(struct bus_type *bus,
			     const char *buf, size_t count)
{
	struct platform_device *pdev;
	struct device *dev;

	dev = bus_find_device_by_name(&platform_bus_type, NULL, buf);
	if (!dev)
		return -ENODEV;
	pdev = to_platform_device(dev);
	/* Check that it's a ddm_device before unregister*/

	platform_device_unregister(pdev);

	return  count;
}
BUS_ATTR(ddm_device_del, 0220, NULL, ddm_device_del_store);


/**
 * It prints all the resources potentially available for new devices
 * check also /proc/iomem and /proc/interrupts to see which rescources
 * have been already taken.
 */
ssize_t ddm_resources_show(struct bus_type *bus, char *buf)
{
	struct ddm_resource *cur;
	ssize_t count = 0;

	buf[0] = 0;
	list_for_each_entry(cur, &ddm->list_base_res, list) {
		count = snprintf(buf, PAGE_SIZE,
				 "%s%s - %s.%d (%s) %pr\n",
				 buf, dev_name(cur->parent),
				 cur->id.name, cur->id.id,
				 dev_name(cur->parent),
				 cur->res);
	}
	return count;
}
BUS_ATTR(ddm_resources, 0444, ddm_resources_show, NULL);


/**
 * It adds a set of foundamental resources to the DDM. The resources
 * will be assigned to sub-devices
 */
int ddm_resource_add(struct ddm_resource *res)
{
	struct ddm_resource *cur;

	if (!res || !res->parent)
		return -EINVAL;

	/* check if it already exists */
	list_for_each_entry(cur, &ddm->list_base_res, list) {
		if (&cur->list == &res->list) {
			dev_err(res->parent,
				"Resources \"%s.%d\" already exported\n",
				res->id.name, res->id.id);
			return -EINVAL;
		}
	}

	spin_lock(&ddm->lock);
	list_add(&res->list, &ddm->list_base_res);
	spin_unlock(&ddm->lock);

	return 0;
}
EXPORT_SYMBOL(ddm_resource_add);


/**
 * Remove a resource previously exported through the DDM
 */
int ddm_resource_del(struct ddm_resource *res)
{
	struct ddm_resource *cur;

	if (!res)
		return 0;

	spin_lock(&ddm->lock);
	list_for_each_entry(cur, &ddm->list_base_res, list) {
		if (&cur->list == &res->list) {
			list_del(&res->list);
			break;
		}
	}
	spin_unlock(&ddm->lock);

	return 0;
}
EXPORT_SYMBOL(ddm_resource_del);


static int __init ddm_init(void)
{
	int err;

	spin_lock_init(&ddm->lock);
	INIT_LIST_HEAD(&ddm->list_base_res);

	err = bus_create_file(&platform_bus_type, &bus_attr_ddm_device_add);
	if (err)
		goto out_add;
	err = bus_create_file(&platform_bus_type, &bus_attr_ddm_device_del);
	if (err)
		goto out_del;
	err = bus_create_file(&platform_bus_type, &bus_attr_ddm_resources);
	if (err)
		goto out_res;
	return 0;

out_res:
	bus_remove_file(&platform_bus_type, &bus_attr_ddm_device_del);
out_del:
	bus_remove_file(&platform_bus_type, &bus_attr_ddm_device_add);
out_add:
	return err;
}

static void __exit ddm_exit(void)
{
	struct ddm_resource *cur, *tmp;

	bus_remove_file(&platform_bus_type, &bus_attr_ddm_resources);
	bus_remove_file(&platform_bus_type, &bus_attr_ddm_device_del);
	bus_remove_file(&platform_bus_type, &bus_attr_ddm_device_add);

	/* Remove all registerd resources */
	list_for_each_entry_safe(cur, tmp, &ddm->list_base_res, list) {
		spin_lock(&ddm->lock);
		list_del(&cur->list);
		spin_unlock(&ddm->lock);
	}
}

module_init(ddm_init);
module_exit(ddm_exit);

MODULE_AUTHOR("Federico Vaga <federico.vaga@cern.ch>");
MODULE_LICENSE("GPL");
MODULE_VERSION(GIT_VERSION);
MODULE_DESCRIPTION("Device Delivery Module - register devices from userspace");
