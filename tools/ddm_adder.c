/*
 * Copyright CERN 2016, GNU GPL 3 or later.
 * Author: Federico vaga <federico.vaga@cern.ch>
 */
#include <stdio.h>
#include <libddm.h>

int main(int argc, char *argv[])
{
	struct ddm_device_user dev_vic = {
		.name = "htvic-svec",
		.id = 0,
		.res = {
			{
				.id = {"htvic-base", 0},
				.parent = {"svec-base", 0},
				.start = 0x2000,
				.end = 0x20FF,
				.flags = 0x200,
			}, {
				.id = {"htvic-irq", 0},
				.parent = {"svec-irq", 0},
				.start = 0,
				.end = -1,
				.flags = 0x400,
			},
			{{"", 0}, {"", 0},  0,0, 0}, /* last one */
		},
		.priv = {1,2,3,4},
	};
	struct ddm_device_user dev_trtl = {
		.name = "mock-turtle-svec",
		.id = 0,
		.res = {
			{
				.id = {"htvic-base", 0},
				.parent = {"svec-base", 0},
				.start = 0x20000,
				.end = 0x2FFFF,
				.flags = 0x200,
			}, {
				.id = {"trtl-irq-hmq", 0},
				.parent = {"htvic-svec.0-irq", 0},
				.start = 0,
				.end = -1,
				.flags = 0x400,
			}, {
				.id = {"trtl-irq-dbg", 0},
				.parent = {"htvic-svec.0-irq", 1},
				.start = 0,
				.end = -1,
				.flags = 0x400,
			},
			{{"", 0}, {"", 0},  0,0, 0}, /* last one */
		},
		.priv = {1,2,3,4},
	};
	int err;
	char a[10], c;
	int i, y;

	err = ddm_device_register(&dev_vic);
	err = ddm_device_register(&dev_trtl);

	/* err = sscanf("prova.4 4", "%[^.]%[.]%d %d", a, &c, &i, &y); */
	/* printf("%d %s %d %d\n", err, a, i, y); */

	return err;
}
