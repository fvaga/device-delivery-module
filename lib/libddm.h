/*
 * Copyright CERN 2016, GNU GPL 3 or later.
 * Author: Federico vaga <federico.vaga@cern.ch>
 */

#ifndef __LIBDDM_H__
#define __LIBDDM_H__
#include <ddm-user.h>

extern int ddm_device_register(struct ddm_device_user *ddm_dev);

#endif
