/*
 * Copyright CERN 2016, GNU GPL 3 or later.
 * Author: Federico vaga <federico.vaga@cern.ch>
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "libddm.h"

const char *libddm_version = GIT_VERSION;


/**
 * It registers a given ddm_device on the platform bus
 * @param[in] ddm_dev DDM device instance
 * @return 0 on success otherwise -1 and errno i appropriately set
 */
int ddm_device_register(struct ddm_device_user *ddm_dev)
{
	int fd, ret = 0;

	fd = open("/sys/bus/platform/ddm_device_add", O_WRONLY);
	if (fd < 0)
		return -1;

	ret = write(fd, ddm_dev, sizeof(struct ddm_device_user));

	close(fd);
	return ret != sizeof(struct ddm_device_user) ? -1 : 0;
}
